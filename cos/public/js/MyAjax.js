
$(document).ready(function () {
    $("#Pais").change(function () {
        $.ajax({
            type: "get",
            url: "/departamentos/" + $("#Pais").val(),
            success: function (resultado) {
                var datos = "";
                for (var value in resultado["departamento"]) {
                    datos += "<option value='" + resultado["departamento"][value].id_Departamento + "'>" + resultado["departamento"][value].nombre_departamento + "</option>\n"
                }
                $("#departamento").html(datos);
            },
            error: function (resultado) {
                console.log(resultado);
            }
        });
    });
});