<?php
use RealRashid\SweetAlert\Facades\Alert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('clientes', 'ClientesController');



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


//rutas de cliente

//Route::post('/clientes', 'ClientesController@store')->name('cliente.store');
Route::get('/cliente', 'ClientesController@index')->name('cliente');
Route::delete('/cliente/{idCliente}', 'ClientesController@destroy')->name('eliminar-cliente');

//Route::get('/cliente/{idCliente}/edit', 'ClientesController@edit')->name('editar-cliente')->middleware('auth');

Route::post('/cliente/{idCliente}/editar','ClientesController@update')->name('cliente.update');

Route::get('/cliente-agregar','ClientesController@create')->name('agregar-cliente')->middleware('auth');

Route::post('/clientes/agregar','ClientesController@store')->name('agregarCliente');

//rutas de departamentos
Route::get('/departamentos/{idCliente}', 'ClientesController@departamentoAjax');
Route:: get ("/MyAjax/{idCliente}", "ClientesController@MyAjax");


