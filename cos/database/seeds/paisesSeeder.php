<?php

use App\Paises;
use Illuminate\Database\Seeder;

class paisesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   
    public function run()
    {

        $pai = [
            'Colombia',
            'Peru',
            'Mexico',
            'Argentina'

        ];
        foreach ($pai as $key => $value) {
            paises::create([
                'nombrePais' => $value
            ]);
        }
    }
}
