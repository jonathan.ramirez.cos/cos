<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class clienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $faker = Faker::create();
            DB::table('clientes')->insert([
                'nombreCliente' => $faker->firstName,
                'apellidoCliente' => $faker->lastName,
                'telefonoCliente' => $faker->numberBetween($min = 3000000000, $max = 4000000000),
                'id_Paises' => App\Paises::all()->random()->idPaises,
                'id_sexo' => App\Sexo::all()->random()->id,
                'id_Departamento' => App\departamentos::all()->random()->id_Departamento,
                'usIdUsuario'=> App\User::all()->random()->id,
            ]);
        }
    }
}
