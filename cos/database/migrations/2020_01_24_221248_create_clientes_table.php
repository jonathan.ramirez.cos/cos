<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('idCliente');
            $table->string('nombreCliente');
            $table->string('apellidoCliente');
            $table->String('telefonoCliente');
            $table->unsignedBigInteger('usIdUsuario');
            $table->foreign('usIdUsuario', 'usuarioidusuario')->references('id')->on('users')->onDelete('restrict')->onupdate('restrict');
            $table->unsignedBigInteger('id_Paises');
            $table->foreign('id_Paises', 'paisIdPais')->references('idPaises')->on('paises')->onDelete('restrict')->onupdate('restrict');
            $table->unsignedBigInteger('id_Departamento');
            $table->foreign('id_Departamento', 'departamentoId')->references('id_Departamento')->on('departamentos')->onDelete('restrict')->onupdate('restrict');
            $table->unsignedBigInteger('id_sexo');
            $table->foreign('id_sexo','fk_cliente_sexo')->references('id')->on('sexo')->onUpdate('retrict')->onUpdate('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
