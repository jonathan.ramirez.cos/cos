@extends('layouts.app')

@section('content')

@inject('users', 'App\Services\Users')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    cliente
                </div>
                @if(session('flash'))
                <br>
                <div class="alert alert-success" role="alert">
                    <strong>Aviso:</strong>{{session('flash')}}
                    <button type="button" class="close" data-dismiss="alert" alert-lable="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if(session('success_message'))
                <br>
                <div class="alert alert-success">
                    {{ session('success_message')}}

                </div>
                @endif
                <ul class=" list-group list-group-flush">
                    <li class="list-group-item"> 
                        <a href="{{ route('agregar-cliente') }}">agregar</a>
                    </li>
                </ul>
            
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_id" class="table table-striped" >
                            <thead class="thead-light">
                                <th>Nombre del cliente</th>
                                <th>apellido del cliente</th>
                                <th>sexo del cliente</th>
                                <th>Teléfono del cliente</th>
                                <th>Corre</th>
                                <th>Pais</th>
                                <th>Departamento</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                @foreach ($clientes as $cliente)
                                <tr>
                                    <td>{{ $cliente->nombreCliente }}</td>
                                    <td>{{ $cliente->apellidoCliente }}</td>
                                    <td>{{ $cliente->sexo->nombre_sexo }}</td>
                                    <td>{{ $cliente->telefonoCliente}}</td>
                                    <td>{{ $cliente->user->email }}</td>
                                    <td>{{ $cliente->paises->nombrePais }}</td>
                                    <td>{{ $cliente->departamentos->nombre_departamento}}</td>
                                    <td>
                                        <a href="{{ route('clientes.edit',$cliente->idCliente) }}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <form  action="{{ route('eliminar-cliente',['idCliente' =>$cliente->idCliente]) }}"class="d-inline form-eliminar" method="POST" >
                                            @csrf @method("delete")
                                            <button type="submit" class="btn-accion-tabla" title="eliminar este registro" >
                                                <i class="fa fa-fw fa-trash text-danger" value="eliminar"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@include('sweetalert::alert')
@endsection
@section('script')
<script>
      
  $(document).ready(function () {
    $('#table_id').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
  });
</script>
    
@endsection