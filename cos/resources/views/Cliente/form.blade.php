@extends('layouts.app')

@section('content')
@inject('paises', 'App\Services\Paise')
@inject('users', 'App\Services\Users')
@inject('sexo', 'App\Services\Sexos')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar cliente</div>

                <div class="card-body">


                    <h1 class="text-center ">Registrate</h1>
                    <form action="{{ route('cliente.store')}}" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{ csrf_field()}}
                                <label for="nombreCliente">Nombre</label>
                                <input type="text" class="form-control" id="nombreCliente" name="nombreCliente">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidoCliente">Apellido</label>
                                <input type="text" class="form-control" id="apellidoCliente" name="apellidoCliente">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="sexoCliente">Sexo</label>
                                    <select name="id_sexo" id="id_sexo" class="form-control">
                                        @foreach ($sexo ->get() as $index => $sex)
                                        <option value="{{ $index }}" {{ old('id_sexo') == $index ? 'selected' : '' }}>
                                            {{$sex}}
                                        </option>
                                        @endforeach
    
                                    </select>
                                    @if ($errors->has('id_sexo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_sexo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            
                            <div class="form-group col-md-6">
                                <label for="telefonoCliente">Teléfono</label>
                                <input type="number" class="form-control" id="telefonoCliente" name="telefonoCliente">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Pais">País donde reside</label>
                                <select name="id_Paises" id="Pais" class="form-control">
                                    @foreach ($paises ->get() as $index => $pais)

                                    <option value="{{ $index }}" {{ old('id_Paises') == $index ? 'selected' : '' }}>
                                        {{$pais}}
                                    </option>
                                    @endforeach

                                </select>
                                @if ($errors->has('id_Paises'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('id_Paises') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="departamento">departamento</label>


                                <select id="departamento" data-old="{{ old('id_Departamento') }}" name="id_Departamento" class="form-control{{ $errors->has('id_Departamento') ? ' is-invalid' : '' }}"></select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="usIdUsuario">Usuario</label>
                                <select name="usIdUsuario" id="User" class="form-control">
                                    @foreach ($users ->get() as $index => $user)

                                    <option value="{{ $index }}" {{ old('usIdUsuario') == $index ? 'selected' : '' }}>
                                        {{$user}}
                                    </option>
                                    @endforeach

                                </select>


                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary btn-lg btn-block">Registrar</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection