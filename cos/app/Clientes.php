<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class clientes extends Model
{
    protected $table="clientes";

    protected $primaryKey = "idCliente";
    protected $fillable = ['nombreCliente','apellidoCliente','telefonoCliente','usIdUsuario','id_Paises','id_Departamento','id_sexo'];
    
    public function sexo(){
        return $this->belongsTo(Sexo::class,'id_sexo');
    }
    public function user(){
        return $this->belongsTo(User::class,'usIdUsuario');
    }
    public function paises(){
        return $this->belongsTo(Paises::class,'id_Paises');
    }
    public function departamentos(){
        return $this->belongsTo(departamentos::class,'id_Departamento');
    }
    public function selectDepartamento($id_pais){
        $query=DB::select("SELECT idPaises, nombrePais FROM paises ORDER by nombrePais ASC");
        return $query;
    }
}
