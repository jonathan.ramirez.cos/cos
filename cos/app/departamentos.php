<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class departamentos extends Model
{
    protected $table = 'departamentos';
    protected $primaryKey = "id_Departamento";
    protected $fillable = ['nombre_departamento','id_Paises'];

    public function cliente()
    {
        return $this->HasMany(clientes::class);
    }
    public function consulta(){
        departamentos::select('SELECT * from departamentos INNER join paises on departamentos.id_Paises= paises.idPaises WHERE idPaises');
    }

}
