<?php 

namespace App\Services;

use App\departamentos;

class Departamento
{

    public function get()
    {
        $depart = departamentos::get();
        $departamentoarray[''] = 'Selecciona un departamento';
        foreach($depart as $depa){
            $departamentoarray[$depa->id_Departamento]=$depa->nombre_departamento;

        }
        return $departamentoarray;
    }

}