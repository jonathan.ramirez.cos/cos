<?php 

namespace App\Services;

use App\Paises;

class Paise
{

    public function get()
    {
        $paises = Paises::get();
        $paisesarray[''] = 'Selecciona un pais';
        foreach($paises as $pais){
            $paisesarray[$pais->idPaises]=$pais->nombrePais;

        }
        return $paisesarray;
    }

}