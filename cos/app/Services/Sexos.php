<?php 

namespace App\Services;

use App\Sexo;

class Sexos
{

    public function get()
    {
        $sexs = Sexo::get();
        $sexosarray[''] = 'Selecciona un sexo';
        foreach($sexs as $sex){
            $sexosarray[$sex->id]=$sex->nombre_sexo;

        }
        return $sexosarray;
    }

}