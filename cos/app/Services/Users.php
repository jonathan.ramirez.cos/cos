<?php 

namespace App\Services;

use App\User;

class Users
{

    public function get()
    {
        $users = User::get();
        $usersarray[''] = 'Selecciona un usuario';
        foreach($users as $user){
            $usersarray[$user->id]=$user->email;

        }
        return $usersarray;
    }

}