<?php

namespace App\Http\Controllers;

use App\departamentos;
use App\paises;
use Illuminate\Http\Request;

class PaisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $state = paises::list('nombrePais', 'idPaises');
        return view('index',compact('index'));
         
    }

     public function getDepartamentos(Request $request, $id){
        if($request->ajax()){
            $departamentos = departamentos::departamentos($id);
            return response()->json($departamentos);
        }
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function show(paises $paises)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function edit(paises $paises)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, paises $paises)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function destroy(paises $paises)
    {
        //
    }
}
