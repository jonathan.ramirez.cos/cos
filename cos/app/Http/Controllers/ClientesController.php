<?php

namespace App\Http\Controllers;


//modelos
use App\Clientes;
use App\departamentos;
use App\Http\Requests\ClienteStoreRequest;
use App\Http\Requests\ClienteUpdateRequest;
use App\Paises;
use App\Sexo;
use App\User;
//
use Illuminate\Support\Facades\DB;
//request
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\Console\Input\Input;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $clientes = clientes::orderBy('idCliente')->get();
        $clientes->each(function ($clientes) {
            $clientes->sexo;
            $clientes->user;
            $clientes->paises;
            $clientes->departamentos;
        });
        return view('Cliente.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cliente.create');
    }
    public function crear()
    {
        return view('Cliente.create', compact('paises'));
        return view('Cliente.create', compact('users'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteStoreRequest  $request)
    {
        //creacion de cliente

        Clientes::create($request->all());
        alert()->success('El cliente fue registrado con exito');
        return redirect()->route('cliente')->with('success', ' El cliente fue registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */

    public function edit($idCliente)
    {
        //ciente
        $clientes = Clientes::find($idCliente);
        //sexo
        $sex = Sexo::all();
        $id_Sexo = $clientes->id_Sexo;
        $tipoSexo = Sexo::find($id_Sexo);
        //pais
        $pais = Paises::all();
        $idPaises = $clientes->id_Paises;
        $nombrePais = Paises::find($idPaises);
        //departamento
        $departamentosDP = departamentos::where('id_Paises',  $idPaises)->get();
        $id_Departamento = $clientes->id_Departamento;
        $nombreDepartamento = departamentos::find($id_Departamento);

        //retorno de datos
        return view('Cliente.edit')
            //cliente
            ->with('clientes', $clientes)
            //sexo
            ->with('sexss', $sex)
            ->with('tiposSexo', $tipoSexo)
            //pais
            ->with('paisess', $pais)
            ->with('nombrePais', $nombrePais)
            ->with('idPaises', $idPaises)
            //departamento
            ->with('departamentosDP', $departamentosDP)
            ->with('nombreDepartamento', $nombreDepartamento)
            ->with('idDepartamento', $id_Departamento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteUpdateRequest $request, $idCliente)
    {
        // $clienteD= request()->extends(['_token','_method']);
        $cliente = Clientes::find($idCliente)->update($request->all());
        alert()->success('Datos actualizados con éxito', 'Listo');
        return redirect('cliente')->with('status', 'Datos actualizados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Clientes::destroy($id);
        return redirect('cliente');
    }
    public function getDepartamentos(Request $request)
    {
        if ($request->ajax()) {


            $departamentos = departamentos::where('id_Paises', $request->id_Paises)->get();

            foreach ($departamentos as $departamento) {


                $departamentosArray[$departamento->id_Departamento] = $departamento->nombre_departamento;
            }
            return response()->json($departamentosArray);
        }
    }
    public function departamentoAjax($idCliente)
    {
        $departamentos["departamento"] = departamentos::where('id_Paises', $idCliente)->get();
        return Response()->json($departamentos, 200);
    }
}
