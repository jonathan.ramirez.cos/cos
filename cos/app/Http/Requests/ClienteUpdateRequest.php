<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombreCliente' => 'required|alpha|max:50',
            'apellidoCliente' => 'required|alpha|max:50',
            'telefonoCliente' => 'required|regex:/^[0-9]+$/|min:10|max:10|unique:clientes',
            'usIdUsuario' => 'required|exists:users,id',
            'id_Paises' => 'required|exists:paises,idPaises',
            'id_Departamento' => 'required|exists:departamentos,id_Departamento',
            'id_sexo' => 'required|exists:sexo,id'
        ];
    }
    public function attributes()
    {
        return [
            'nombreCliente' => 'Nombre del cliente',
            'apellidoCliente' => 'Apellido del cliente',
            'telefonoCliente' => 'Teléfono del cliente',
            'usIdUsuario' => 'Usuario',
            'id_Paises' => 'Pais donde reside',
            'id_Departamento' => 'Departamento donde reside',
            'Sexo' => 'sexo',
        ];
    }
}
