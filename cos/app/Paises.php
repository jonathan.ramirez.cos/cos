<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    protected $table = 'paises';
    protected $primaryKey = "idPaises";
    protected $fillable = ['nombrePais']; 
    
    public function cliente(){
        return $this->HasMany(clientes::class);
    }

}